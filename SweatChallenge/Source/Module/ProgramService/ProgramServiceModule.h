//
//  ProgramServiceModule.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 22/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@protocol Trainer <NSObject>

@property (nonatomic, readonly) NSInteger identifier;
@property (nonatomic, copy, readonly) NSString *codeName;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSString *imageUrlString;

@end

@protocol Tag <NSObject>

@property (nonatomic, readonly) NSInteger identifier;
@property (nonatomic, copy, readonly) NSString *name;

@end


@protocol ProgramAttribute <NSObject>

@property (nonatomic, readonly) NSInteger identifier;
@property (nonatomic, copy, readonly) NSString *codeName;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, readonly) double value;
@property (nonatomic, readonly) double total;

@end

@protocol ProgramPreview <NSObject>

@property (nonatomic, readonly) NSInteger identifier;
@property (nonatomic, copy, readonly) NSString *acronym;
@property (nonatomic, copy, readonly) NSString *codeName;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSString *imageUrlString;
@property (nonatomic, copy, readonly) NSArray<ProgramAttribute> *attributes;
@property (nonatomic, copy, readonly) NSArray<Tag> *tags;
@property (nonatomic, copy, readonly) id<Trainer> trainer;

@end

/// a protocol that describes an entity responsible for program-related operations
@protocol ProgramService <NSObject>

typedef void (^ProgramPreviewResponseBlock)(NSArray<ProgramPreview> *);

/// fetch a list of reccommended programs
/// @param block a completion block that is triggered once the list is recieved from the underlying storage
- (void)requestRecommendedProgramsWithCompletionBlock:(ProgramPreviewResponseBlock)block;

@end

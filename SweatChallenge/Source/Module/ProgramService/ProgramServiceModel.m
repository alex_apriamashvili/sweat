//
//  ProgramServiceModel.m
//  ProgramService
//
//  Created by Alex Apriamashvili on 22/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "ProgramServiceModel.h"

@implementation ProgramPreviewModel

@synthesize identifier, name, codeName, acronym, imageUrlString, tags, trainer, attributes;

+ (instancetype)decodeWithData:(NSDictionary *)data {
  ProgramPreviewModel *model = [ProgramPreviewModel new];
  
  model->identifier     = [(NSNumber *)data[@"id"] integerValue];
  model->acronym        = data[@"acronym"];
  model->codeName       = data[@"code_name"];
  model->name           = data[@"name"];
  model->imageUrlString = data[@"image"];
  model->attributes     = [self mapAttributesWithData:data[@"attributes"]];
  model->tags           = [self mapTagWithData:data[@"tags"]];
  model->trainer        = [TrainerModel decodeWithData:data[@"trainer"]];
  
  return model;
}

+ (NSArray<ProgramAttribute> *)mapAttributesWithData:(NSArray *)data {
  NSMutableArray *attributes = [NSMutableArray array];
  for (NSDictionary *attr in data) {
    [attributes addObject:[ProgramAttributeModel decodeWithData:attr]];
  }
  return [attributes copy];
}

+ (NSArray<Tag> *)mapTagWithData:(NSDictionary *)data {
  NSMutableArray *tags = [NSMutableArray array];
  for (NSDictionary *tag in data) {
    [tags addObject:[TagModel decodeWithData:tag]];
  }
  return [tags copy];
}

@end

@implementation ProgramAttributeModel

@synthesize identifier, codeName, name, value, total;

+ (instancetype)decodeWithData:(NSDictionary *)data {
  ProgramAttributeModel *attr = [ProgramAttributeModel new];
  
  attr->identifier  = [(NSNumber *)data[@"id"] integerValue];
  attr->codeName    = data[@"code_name"];
  attr->name        = data[@"name"];
  attr->value       = [(NSNumber *)data[@"value"] doubleValue];
  attr->total       = [(NSNumber *)data[@"total"] doubleValue];
  
  return attr;
}

@end

@implementation TrainerModel

@synthesize identifier, codeName, name, imageUrlString;

+ (instancetype)decodeWithData:(NSDictionary *)data {
  TrainerModel *trainer = [TrainerModel new];
  
  trainer->identifier     = [(NSNumber *)data[@"id"] integerValue];
  trainer->codeName       = data[@"code_name"];
  trainer->name           = data[@"name"];
  trainer->imageUrlString = data[@"image_url"];
  
  return trainer;
}

@end

@implementation TagModel

@synthesize identifier, name;

+ (instancetype)decodeWithData:(NSDictionary *)data {
  TagModel *tag   = [TagModel new];
  
  tag->identifier = [(NSNumber *)data[@"id"] integerValue];
  tag->name       = data[@"name"];
  
  return tag;
}

@end

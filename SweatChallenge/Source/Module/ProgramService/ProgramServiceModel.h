//
//  ProgramServiceModel.h
//  ProgramService
//
//  Created by Alex Apriamashvili on 22/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import Foundation;
#import "ProgramServiceModule.h"

/// a protocol that defines an entity that can be initialized with a raw NSDictionary data
@protocol SWTDecodable <NSObject>

/// initialize an entity by decoding an instance of NSDictionary received
/// @param data a dictionary that contains necessary information
+ (instancetype)decodeWithData:(NSDictionary *)data;

@end

NS_ASSUME_NONNULL_BEGIN

@interface ProgramPreviewModel : NSObject<ProgramPreview, SWTDecodable>

@end

@interface ProgramAttributeModel : NSObject<ProgramAttribute, SWTDecodable>

@end

@interface TrainerModel : NSObject<Trainer, SWTDecodable>

@end

@interface TagModel : NSObject<Tag, SWTDecodable>

@end

NS_ASSUME_NONNULL_END

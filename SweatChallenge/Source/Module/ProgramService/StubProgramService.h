//
//  StubProgramService.h
//  ProgramService
//
//  Created by Alex Apriamashvili on 22/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import Foundation;
#import "ProgramServiceModule.h"

NS_ASSUME_NONNULL_BEGIN

@interface StubProgramService : NSObject<ProgramService>

@end

NS_ASSUME_NONNULL_END

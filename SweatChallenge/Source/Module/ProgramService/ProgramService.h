//
//  ProgramService.h
//  ProgramService
//
//  Created by Alex Apriamashvili on 22/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ProgramService.
FOUNDATION_EXPORT double ProgramServiceVersionNumber;

//! Project version string for ProgramService.
FOUNDATION_EXPORT const unsigned char ProgramServiceVersionString[];

#import <ProgramService/ProgramServiceModule.h>
#import <ProgramService/ProgramServiceModel.h>
#import <ProgramService/StubProgramService.h>


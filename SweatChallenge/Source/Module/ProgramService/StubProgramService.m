//
//  StubProgramService.m
//  ProgramService
//
//  Created by Alex Apriamashvili on 22/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "StubProgramService.h"
#import "ProgramServiceModel.h"

NSString * const _kStubProgramService_StubsBundleName = @"StubResources";
NSString * const _kStubProgramService_BundleExtension = @"bundle";
NSString * const _kStubProgramService_JSONExtension = @"json";

@implementation StubProgramService

- (void)requestRecommendedProgramsWithCompletionBlock:(ProgramPreviewResponseBlock)block {
  NSArray<NSDictionary *> *rawData = [self parseJSONStub];
  NSMutableArray *parsedResults = [NSMutableArray array];
  for (NSDictionary *data in rawData) {
    [parsedResults addObject:[ProgramPreviewModel decodeWithData:data]];
  }
  block([parsedResults copy]);
}

- (NSArray<NSDictionary *> *)parseJSONStub {
  NSString* jsonString = [self stubJSONStringInFileNamed:@"trainer-programs"];
  NSData* jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
  NSError *jsonError;
  return [NSJSONSerialization JSONObjectWithData:jsonData
                                         options:NSJSONReadingFragmentsAllowed
                                           error:&jsonError];
}

- (NSString *)stubJSONStringInFileNamed:(NSString *)filename {
  NSString *stubsBundlePath = [[NSBundle mainBundle] pathForResource:_kStubProgramService_StubsBundleName
                                                              ofType:_kStubProgramService_BundleExtension];
  NSBundle *stubsBandle = [NSBundle bundleWithPath:stubsBundlePath];
  NSString* path = [stubsBandle pathForResource:filename ofType:_kStubProgramService_JSONExtension];
  return [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

@end

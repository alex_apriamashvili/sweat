//
//  ProgramServiceTests.m
//  ProgramServiceTests
//
//  Created by Alex Apriamashvili on 22/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ProgramServiceModel.h"
#import "PreviewDictionaryStub.h"

@interface ProgramPreviewMappingTests : XCTestCase

@end

@implementation ProgramPreviewMappingTests

- (void)testProgramPreviewModelMapping {
  ProgramPreviewModel *sut = [ProgramPreviewModel decodeWithData:[PreviewDictionaryStub stub]];
  
  XCTAssertEqual(1, sut.identifier);
  XCTAssertEqual(@"BBG", sut.acronym);
  XCTAssertEqual(@"kayla", sut.codeName);
  XCTAssertEqual(@"BBG", sut.name);
  XCTAssertEqual(@"https://assets.sweat.com", sut.imageUrlString);
  XCTAssertEqual(2, sut.attributes.count);
  XCTAssertNotNil(sut.trainer);
  XCTAssertEqual(1, sut.tags.count);
}

- (void)testProgramAttributeModelMapping {
  ProgramPreviewModel *preview = [ProgramPreviewModel decodeWithData:[PreviewDictionaryStub stub]];
  ProgramAttributeModel *sut = preview.attributes.firstObject;
  
  XCTAssertEqual(1, sut.identifier);
  XCTAssertEqual(@"intensity", sut.codeName);
  XCTAssertEqual(@"Intensity", sut.name);
  XCTAssertEqualWithAccuracy(2, sut.value, 0.001);
  XCTAssertEqualWithAccuracy(3, sut.total, 0.001);
}

- (void)testTrainerModelMapping {
  ProgramPreviewModel *preview = [ProgramPreviewModel decodeWithData:[PreviewDictionaryStub stub]];
  TrainerModel *sut = (TrainerModel *)preview.trainer;
  
  XCTAssertEqual(2, sut.identifier);
  XCTAssertEqual(@"kayla", sut.codeName);
  XCTAssertEqual(@"Kayla Itsines", sut.name);
  XCTAssertEqual(@"https://assets.sweat.com/trainers/", sut.imageUrlString);
}

- (void)testTagModelMapping {
  ProgramPreviewModel *preview = [ProgramPreviewModel decodeWithData:[PreviewDictionaryStub stub]];
  TagModel *sut = (TagModel *)preview.tags.firstObject;
  
  XCTAssertEqual(129, sut.identifier);
  XCTAssertEqual(@"Equipment Needed", sut.name);
}

@end

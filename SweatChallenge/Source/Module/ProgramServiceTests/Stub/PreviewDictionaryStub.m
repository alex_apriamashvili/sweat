//
//  PreviewDictionaryStub.m
//  ProgramServiceTests
//
//  Created by Alex Apriamashvili on 04/01/2020.
//  Copyright © 2020 Alex Apriamashvili. All rights reserved.
//

#import "PreviewDictionaryStub.h"

@implementation PreviewDictionaryStub

+ (NSDictionary *)stub {
  return @{
    @"id": @1,
    @"acronym": @"BBG",
    @"code_name": @"kayla",
    @"name": @"BBG",
    @"image": @"https://assets.sweat.com",
    @"attributes": @[
        @{
            @"id": @1,
            @"code_name": @"intensity",
            @"name": @"Intensity",
            @"value": @"2.0",
            @"total": @"3.0"
        },
        @{
            @"id": @2,
            @"code_name": @"weight loss",
            @"name": @"Weight Loss",
            @"value": @"5.0",
            @"total": @"5.0"
        },
    ],
    @"trainer": @{
        @"id": @2,
        @"name": @"Kayla Itsines",
        @"code_name": @"kayla",
        @"image_url": @"https://assets.sweat.com/trainers/"
    },
    @"tags": @[
        @{
            @"id": @129,
            @"name": @"Equipment Needed"
        },
    ]
  };
}

@end

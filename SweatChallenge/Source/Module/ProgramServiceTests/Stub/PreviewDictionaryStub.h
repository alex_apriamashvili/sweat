//
//  PreviewDictionaryStub.h
//  ProgramServiceTests
//
//  Created by Alex Apriamashvili on 04/01/2020.
//  Copyright © 2020 Alex Apriamashvili. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PreviewDictionaryStub : NSObject

+ (NSDictionary *)stub;

@end

NS_ASSUME_NONNULL_END

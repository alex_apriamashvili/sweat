//
//  SWTColor.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StyleModule.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWTColorImp : NSObject<SWTColor>

@end

NS_ASSUME_NONNULL_END

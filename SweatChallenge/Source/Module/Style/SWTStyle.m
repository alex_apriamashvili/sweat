//
//  SWTStyle.m
//  Style
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTStyle.h"
#import "SWTColor.h"
#import "SWTFont.h"

@interface SWTStyleImp()

@property (nonatomic, strong) id<SWTColor> _color;
@property (nonatomic, strong) id<SWTFont> _font;

@end

@implementation SWTStyleImp

- (instancetype)init {
  self = [super init];
  if (self) {
    __color = [SWTColorImp new];
    __font = [SWTFontImp new];
  }
  return self;
}

- (id<SWTColor>)color {
  return self._color;
}

- (id<SWTFont>)font {
  return self._font;
}

@end

//
//  SWTFont.m
//  Style
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTFont.h"

NSString * const _SWTFontImp_kRegularFontName = @"OpenSans-Semibold";
NSString * const _SWTFontImp_kMediumFontName = @"OpenSans-Bold";
NSString * const _SWTFontImp_kBoldFontName = @"Montserrat-Bold";

@implementation SWTFontImp

- (UIFont *)fontOfSize:(CGFloat)size {
  return [self fontOfSize:size weight:kSWTFontWeightRegular];
}

- (UIFont *)fontOfSize:(CGFloat)size weight:(SWTFontWeight)weight {
  NSString *fontName = [self weightToFontName:weight];
  UIFont *innerFont = [UIFont fontWithName:fontName size:size];
  if (@available(iOS 11.0, *)) {
    return [[UIFontMetrics defaultMetrics] scaledFontForFont:innerFont];
  } else {
    return innerFont;
  }
}

- (NSString *)weightToFontName:(SWTFontWeight)weight {
  switch (weight) {
    case kSWTFontWeightMedium:
      return _SWTFontImp_kMediumFontName;
    case kSWTFontWeightBold:
      return _SWTFontImp_kBoldFontName;
      
    default:
      return _SWTFontImp_kRegularFontName;
  }
}

@end

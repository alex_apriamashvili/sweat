//
//  SWTColor.m
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTColor.h"

const CGFloat _SWTColorImp_FullSpectra = 255.0f;

@implementation SWTColorImp

- (UIColor *)black {
  return [self r:36 g:43 b:43];
}

- (UIColor *)pink {
  return [self r:228 g:50 b:113];
}

- (UIColor *)gray {
  return [self r:141 g:141 b:141];
}

- (UIColor *)lightGray {
  return [self r:159 g:159 b:159];
}

- (UIColor *)veryLightGray {
  return [self r:230 g:230 b:230];
}

- (UIColor *)r:(CGFloat)r g:(CGFloat)g b:(CGFloat)b {
  return [UIColor colorWithRed:r / _SWTColorImp_FullSpectra
                         green: g / _SWTColorImp_FullSpectra
                          blue: b / _SWTColorImp_FullSpectra
                         alpha:1];
}

@end

//
//  StyleModule.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import UIKit;

/// SWEAT font weight
typedef enum : NSUInteger {
  /// regular font weight
  kSWTFontWeightRegular,
  /// medium font weight
  kSWTFontWeightMedium,
  /// bold font weight
  kSWTFontWeightBold,
  /// thin font weight
  kSWTFontWeightThin
} SWTFontWeight;

/// a protocol that describes an entity that could provide an application font
@protocol SWTFont <NSObject>

/// get application font for a given size
/// @param size required font size
/// @returns application font of a required size
- (UIFont *)fontOfSize:(CGFloat)size;

/// get application font for a given size and weight
/// @param size required font size
/// @param weight required font wight degind in `SWTFontWeight`
/// @returns application font of a required size and weight
- (UIFont *)fontOfSize:(CGFloat)size weight:(SWTFontWeight)weight;

@end

/// color palette of the application
@protocol SWTColor <NSObject>

@property (nonatomic, strong, readonly) UIColor *black;
@property (nonatomic, strong, readonly) UIColor *pink;
@property (nonatomic, strong, readonly) UIColor *gray;
@property (nonatomic, strong, readonly) UIColor *lightGray;
@property (nonatomic, strong, readonly) UIColor *veryLightGray;

@end

/// a protocol that lets it witness to expose application defined font book and color palette
@protocol SWTStyle <NSObject>

/// application font book
@property (nonatomic, strong, readonly) id<SWTFont> font;

/// application color palette
@property (nonatomic, strong, readonly) id<SWTColor> color;

@end

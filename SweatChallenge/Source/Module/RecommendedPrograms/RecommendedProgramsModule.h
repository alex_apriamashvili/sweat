//
//  RecommendedProgramsModule.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import UIKit;
#import <Style/Style.h>
#import <ProgramService/ProgramService.h>

/// a protocol that describes an API of `RecommendedPrograms` module,
/// so other modules could communicate with it
@protocol RecommendedProgramsModuleInput <NSObject>

@end

/// an abstract description of  `RecommendedPrograms` module
@protocol RecommendedProgramsModule <NSObject>

/// a view controller that contains `RecommendedPrograms` view hierarchy
@property (readonly, nonatomic) UIViewController *viewController;

/// `RecommendedPrograms` module input that holds module input-communication methods
@property (readonly, nonatomic) id<RecommendedProgramsModuleInput> input;

@end

/// an abstract description of module's dependency
/// any entity that is to initiate this module shall provide an object that implements this protocol, for this module to be built
@protocol RecommendedProgramsDependency <NSObject>

/// application style-set, contains common fonts and colors
@property (readonly, nonatomic) id<SWTStyle> style;

/// program service that serves as a data-source for the entire module
@property (readonly, nonatomic) id<ProgramService> service;

@end

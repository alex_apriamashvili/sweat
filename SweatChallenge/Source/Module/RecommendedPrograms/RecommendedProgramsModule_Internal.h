//
//  RecommendedProgramsModule_Internal.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import UIKit;

/// an abstract description of the 'Characteristics' view-model
/// an entity that represents progress bars for each characteristic on a program tile
@protocol CharacteristicsViewModel <NSObject>

/// characteristic name
@property (readonly, nonatomic, copy) NSString *name;

/// percentage of filling of the progress bar
/// 0.0 – hollow; 0.5 – half-filled; 1.0 – filled
@property (readonly, nonatomic) CGFloat percentage;

@end

/// an abstract description of each tile shown on the RecommendedPrograms screen
@protocol RecommendedBannerViewModel <NSObject>

/// program name
@property (readonly, nonatomic, copy) NSString *title;

/// subtitle (name of a trainer)
@property (readonly, nonatomic, copy) NSString *subtitle;

/// difficulty indicator filling rate ( 0 | 1 | 2 | 3 )
@property (readonly, nonatomic) NSUInteger difficulty;

/// an array of characteristics for the program
/// see `CharacteristicsViewModel`
@property (readonly, nonatomic, copy) NSArray<CharacteristicsViewModel> *characteristics;

/// an array of tags related to the program
@property (readonly, nonatomic, copy) NSArray<NSString *> *tags;

/// a URL to trainer's photo
@property (readonly, nonatomic, copy) NSString *imageURLString;

@end

/// an interface that allows communication between a presenter and a view
/// used to give a simple commands to the view with preformated data
@protocol RecommendedProgramsViewInput <NSObject>

/// reload contents of the collection view with the new set of data
/// @param contents a new set of `RecommendedBannerViewModel` witnesses to be presented on screen
- (void)updateContents:(NSArray<RecommendedBannerViewModel> *)contents;

/// update group title with the localized value
/// @param string new value for the group title
- (void)updateGroupTitle:(NSString *)string;

@end

/// an interface that allows communication between a view and a presenter
/// used to notify preserner about certain events happening on a view
@protocol RecommendedProgramsViewOutput <NSObject>

/// tells presenter that the view is ready to be presented
- (void)viewIsReady;

/// asks presenter to start data fetching
- (void)loadData;

@end

/// an abstract description of UICollectionViewDelegate action handler
/// used to propagate certain events happening on a collection view
@protocol CollectionActionHandler <NSObject>

@end




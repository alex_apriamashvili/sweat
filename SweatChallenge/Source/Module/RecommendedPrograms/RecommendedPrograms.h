//
//  RecommendedPrograms.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for RecommendedPrograms.
FOUNDATION_EXPORT double RecommendedProgramsVersionNumber;

//! Project version string for RecommendedPrograms.
FOUNDATION_EXPORT const unsigned char RecommendedProgramsVersionString[];

#import <RecommendedPrograms/RecommendedProgramsModule.h>
#import <RecommendedPrograms/SWTRecommendedProgramsAssembly.h>


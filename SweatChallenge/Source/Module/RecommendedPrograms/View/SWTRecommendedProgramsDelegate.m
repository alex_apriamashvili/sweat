//
//  SWTRecommendedProgramsDelegate.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Style/Style.h>
#import "RecommendedProgramsModule_Constants.h"
#import "RecommendedProgramsModule_Internal.h"

#import "SWTRecommendedProgramsDelegate.h"
#import "SWTRecommendedProgramsHeader.h"
#import "SWTRecommendationCell.h"
#import "SWTRecommendedItemViewModel.h"
#import "SWTCharacteristicsViewModel.h"

@interface SWTRecommendedProgramsDelegate()

@property (nonatomic, copy) NSArray<RecommendedBannerViewModel> *viewModelList;
@property (nonatomic, weak, readonly) id<CollectionActionHandler> actionHandler;
@property (nonatomic, strong) id<SWTStyle> style;
@property (nonatomic, strong) SWTRecommendationCell *prototypeCell;
@property (nonatomic, copy) NSString *groupTitle;

@end

const NSInteger _kSWTRecommendedProgramsDelegate_NumberOfSections = 1;
const CGFloat _kSWTRecommendedProgramsDelegate_SupplementaryHeight = 48.0f;

@implementation SWTRecommendedProgramsDelegate

+ (instancetype)delegateWithActionHandler:(nullable id<CollectionActionHandler>)actionHandler
                                    style:(id<SWTStyle>)style {
  SWTRecommendedProgramsDelegate *delegate = [SWTRecommendedProgramsDelegate new];
  delegate->_actionHandler = actionHandler;
  delegate->_viewModelList = (NSArray<RecommendedBannerViewModel> *)@[];
  delegate->_style = style;
  return delegate;
}

- (void)updateContents:(NSArray<RecommendedBannerViewModel> *)viewModels {
  _viewModelList = viewModels;
}

- (void)updateGroupTitle:(NSString *)groupTitle {
  _groupTitle = groupTitle;
}

#pragma mark - UICollectionViewDataSource Implementation

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return _kSWTRecommendedProgramsDelegate_NumberOfSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return self.viewModelList.count;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView
                                   cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
  UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RECOMMENDED_PROGRAMS_CELL_IDENTIFIER
                                                                         forIndexPath:indexPath];
  if ([cell isKindOfClass:[SWTRecommendationCell class]]) {
    SWTRecommendationCell *matchedCell = (SWTRecommendationCell *)cell;
    [matchedCell applyViewModel:[self viewModelForIndexPath:indexPath]
                          style:self.style];
  }
  return cell;
}

#pragma mark - UICollectionViewDelegate Implementation

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
  if (kind == UICollectionElementKindSectionHeader) {
    SWTRecommendedProgramsHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                              withReuseIdentifier:RECOMMENDED_PROGRAMS_HEADER_IDENTIFIER
                                                                                     forIndexPath:indexPath];
    [header updateTitle:self.groupTitle style:self.style];
    return header;
  }
  return [UICollectionReusableView new];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section {
  return CGSizeMake(collectionView.bounds.size.width, _kSWTRecommendedProgramsDelegate_SupplementaryHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  id<RecommendedBannerViewModel> viewModel = [self viewModelForIndexPath:indexPath];
  return [self cellSizeWithViewModel:viewModel
                               style:self.style
                               width:collectionView.bounds.size.width];
}

#pragma mark - Size Calculation

- (CGSize)cellSizeWithViewModel:(id<RecommendedBannerViewModel>)viewModel
                          style:(id<SWTStyle>)style
                          width:(CGFloat)width {
  NSLayoutConstraint *widthConstraint =
  [self.prototypeCell.contentView.widthAnchor constraintEqualToConstant:width];
  [widthConstraint setActive:YES];
  
  // Calling `translatesAutoresizingMaskIntoConstraints` will cause warning messages appear in the consile in runtime
  // these warnings could be ignored as those are not causing any issues to the acctual cells
  // that are presented on the screen and applicable to the prototype cell only.
  self.prototypeCell.contentView.translatesAutoresizingMaskIntoConstraints = NO;
  [self.prototypeCell applyViewModel:viewModel style:style];
  
  [self.prototypeCell.contentView setNeedsUpdateConstraints];
  [self.prototypeCell.contentView updateConstraintsIfNeeded];
  [self.prototypeCell.contentView setNeedsLayout];
  [self.prototypeCell.contentView layoutIfNeeded];
  
  CGSize size = [self.prototypeCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
  [widthConstraint setActive:NO];
  [self.prototypeCell removeConstraint:widthConstraint];
  
  return size;
}

#pragma mark - Private

- (SWTRecommendationCell *)prototypeCell {
  if (!_prototypeCell) {
    _prototypeCell = [SWTRecommendationCell new];
  }
  return _prototypeCell;
}

- (id<RecommendedBannerViewModel>)viewModelForIndexPath:(NSIndexPath *)indexPath {
  return self.viewModelList[indexPath.item];
}

@end

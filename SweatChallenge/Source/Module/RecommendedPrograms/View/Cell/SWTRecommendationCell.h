//
//  SWTRecommendationCell.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWTStyle, RecommendedBannerViewModel;

NS_ASSUME_NONNULL_BEGIN

@interface SWTRecommendationCell : UICollectionViewCell

- (void)applyViewModel:(id<RecommendedBannerViewModel>)viewModel
                 style:(id<SWTStyle>)style;

@end

NS_ASSUME_NONNULL_END

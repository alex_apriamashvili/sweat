//
//  SWTCharacteristicsView.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 15/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SWTStyle, CharacteristicsViewModel;

@interface SWTCharacteristicsView : UIView

- (void)updateWithViewModel:(id<CharacteristicsViewModel>)viewModel
                      style:(id<SWTStyle>)style;

@end

NS_ASSUME_NONNULL_END

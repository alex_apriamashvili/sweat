//
//  SWTCharacteristicsStackView.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 15/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CharacteristicsViewModel, SWTStyle;

@interface SWTCharacteristicsStackView : UIView

- (void)updateWithCharacteristics:(NSArray<id<CharacteristicsViewModel>> *)characteristics
                            style:(id<SWTStyle>)style;

@end

NS_ASSUME_NONNULL_END

//
//  SWTDropIndicator.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 14/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWTDropIndicator : UIView

- (void)fillDropsInAmountOf:(NSUInteger)amount;

@end

NS_ASSUME_NONNULL_END

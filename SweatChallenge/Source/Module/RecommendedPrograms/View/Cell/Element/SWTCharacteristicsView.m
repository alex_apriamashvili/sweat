//
//  SWTCharacteristicsView.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 15/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Style/Style.h>
#import "SWTCharacteristicsView.h"
#import "RecommendedProgramsModule_Internal.h"

@interface SWTCharacteristicsView()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *scale;
@property (nonatomic, strong) UIView *filling;
@property (nonatomic, strong) NSLayoutConstraint *fillingRateConstraint;

@end

const CGFloat _kSWTCharacteristicsView_TitleEdgeOffset = 0.0f;
const CGFloat _kSWTCharacteristicsView_FontSize = 12.0f;
const CGFloat _kSWTCharacteristicsView_ScaleHeight = 8.0f;
const CGFloat _kSWTCharacteristicsView_Radii = 4.0f;
const CGFloat _kSWTCharacteristicsView_Element2ElementOffset = 4.0f;

@implementation SWTCharacteristicsView

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self setUp];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super initWithCoder:coder];
  if (self) {
    [self setUp];
  }
  return self;
}

#pragma mark - Public

- (void)updateWithViewModel:(id<CharacteristicsViewModel>)viewModel
                      style:(id<SWTStyle>)style {
  [self applyStyle:style];
  self.titleLabel.text = viewModel.name;
  [self updateFillingRate:viewModel.percentage];
  [self setNeedsLayout];
}
#pragma mark - Private

- (void)setUp {
  [self configureUI];
  [self constrainContents];
}

- (void)configureUI {
  [self configureTitleLabel];
  [self configureScale];
  [self configureFilling];
}

- (void)configureTitleLabel {
  _titleLabel = [UILabel new];
  [self addSubview:self.titleLabel];
}

- (void)configureScale {
  _scale = [UIView new];
  [self addSubview:self.scale];
  self.scale.clipsToBounds = YES;
  self.scale.layer.borderWidth = 1 / [UIScreen mainScreen].scale;
  self.scale.layer.cornerRadius = _kSWTCharacteristicsView_Radii;
}

- (void)configureFilling {
  _filling = [UIView new];
  [self.scale addSubview:self.filling];
  self.filling.clipsToBounds = YES;
  self.filling.layer.cornerRadius = _kSWTCharacteristicsView_Radii;
}

- (void)applyStyle:(id<SWTStyle>)style {
  self.titleLabel.font = [style.font fontOfSize:_kSWTCharacteristicsView_FontSize
                                         weight:kSWTFontWeightRegular];
  self.titleLabel.textColor = style.color.gray;
  self.filling.backgroundColor = style.color.pink;
  self.scale.layer.borderColor = style.color.gray.CGColor;
}


#pragma mark - Private (Layout)

- (void)constrainContents {
  [self constrainTitleLabel];
  [self constrainScale];
  [self constrainFilling];
}

- (void)constrainTitleLabel {
  self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:_kSWTCharacteristicsView_TitleEdgeOffset],
    [self.titleLabel.topAnchor constraintEqualToAnchor:self.topAnchor],
    [self.titleLabel.trailingAnchor constraintLessThanOrEqualToAnchor:self.trailingAnchor constant:-_kSWTCharacteristicsView_TitleEdgeOffset]
  ]];
}

- (void)constrainScale {
self.scale.translatesAutoresizingMaskIntoConstraints = NO;
[NSLayoutConstraint activateConstraints:@[
  [self.scale.heightAnchor constraintEqualToConstant:_kSWTCharacteristicsView_ScaleHeight],
  [self.scale.leadingAnchor constraintEqualToAnchor:self.titleLabel.leadingAnchor],
  [self.scale.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-_kSWTCharacteristicsView_TitleEdgeOffset],
  [self.scale.topAnchor constraintEqualToAnchor:self.titleLabel.bottomAnchor constant:_kSWTCharacteristicsView_Element2ElementOffset],
  [self.scale.bottomAnchor constraintEqualToAnchor:self.bottomAnchor]
]];
}

- (void)constrainFilling {
self.filling.translatesAutoresizingMaskIntoConstraints = NO;
  _fillingRateConstraint = [self.filling.widthAnchor constraintEqualToAnchor:self.scale.widthAnchor multiplier:0.5];
[NSLayoutConstraint activateConstraints:@[
  [self.filling.leadingAnchor constraintEqualToAnchor:self.scale.leadingAnchor],
  [self.filling.topAnchor constraintEqualToAnchor:self.scale.topAnchor],
  [self.filling.bottomAnchor constraintEqualToAnchor:self.scale.bottomAnchor],
  self.fillingRateConstraint,
]];
}

- (void)updateFillingRate:(CGFloat)rate {
  [self.fillingRateConstraint setActive:NO];
  self.fillingRateConstraint = [self.filling.widthAnchor constraintEqualToAnchor:self.scale.widthAnchor multiplier:rate];
  [self.fillingRateConstraint setActive:YES];
}

@end

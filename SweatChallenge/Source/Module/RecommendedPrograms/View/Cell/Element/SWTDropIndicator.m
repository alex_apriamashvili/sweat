//
//  SWTDropIndicator.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 14/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTDropIndicator.h"

@interface SWTDropIndicator()

@property (nonatomic, strong) UIStackView *dropStackView;

@end

const NSUInteger kSWTDropIndicator_NumberOfDrops = 3;
const CGFloat kSWTDropIndicator_Spacing = 6;
NSString * const kSWTDropIndicator_HollowDropImageName = @"sweat-drop";
NSString * const kSWTDropIndicator_FilledDropImageName = @"sweat-drop-filled";

@implementation SWTDropIndicator

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self configureUI];
  }
  return self;
}

#pragma mark - Public

- (void)fillDropsInAmountOf:(NSUInteger)amount {
  [self.dropStackView.arrangedSubviews
   enumerateObjectsUsingBlock:^(UIImageView *obj, NSUInteger idx, BOOL *stop) {
    BOOL isFilled = (idx < amount);
    obj.image = isFilled
    ? [UIImage imageNamed:kSWTDropIndicator_FilledDropImageName]
    : [UIImage imageNamed:kSWTDropIndicator_HollowDropImageName];
  }];
}

#pragma mark - Configuration

- (void)configureUI {
  [self configureStackView];
  [self configureDrops];
}

- (void)configureStackView {
  _dropStackView = [UIStackView new];
  [self addSubview:self.dropStackView];
  self.dropStackView.axis = UILayoutConstraintAxisHorizontal;
  self.dropStackView.distribution = UIStackViewDistributionFillEqually;
  self.dropStackView.alignment = UIStackViewAlignmentCenter;
  self.dropStackView.spacing = kSWTDropIndicator_Spacing;
  
  self.dropStackView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.dropStackView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor],
    [self.dropStackView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor],
    [self.dropStackView.topAnchor constraintEqualToAnchor:self.topAnchor],
    [self.dropStackView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor],
  ]];
}

- (void)configureDrops {
  for (int i = 0; i < kSWTDropIndicator_NumberOfDrops; i++) {
    UIImageView *dropImageContainer = [UIImageView new];
    dropImageContainer.image = [UIImage imageNamed:kSWTDropIndicator_HollowDropImageName];
    [self.dropStackView addArrangedSubview:dropImageContainer];
  }
}

@end

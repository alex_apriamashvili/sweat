//
//  SWTCharacteristicsStackView.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 15/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Style/Style.h>
#import "SWTCharacteristicsStackView.h"
#import "SWTCharacteristicsView.h"

@interface SWTCharacteristicsStackView()

@property (nonatomic, strong) UIStackView *innerStackView;

@end

const CGFloat kSWTCharacteristicsStackView_Spacing = 6;

@implementation SWTCharacteristicsStackView

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self configureUI];
  }
  return self;
}

#pragma mark - Public

- (void)updateWithCharacteristics:(NSArray<id<CharacteristicsViewModel>> *)characteristics
                            style:(id<SWTStyle>)style {
  [self adjustStackSizeWithSize:characteristics.count];
  for (int i = 0; i < characteristics.count; i++) {
    id<CharacteristicsViewModel> characteristic = characteristics[i];
    SWTCharacteristicsView *characteristicView = self.innerStackView.arrangedSubviews[i];
    [characteristicView updateWithViewModel:characteristic style:style];
  }
}

#pragma mark - Configuration

- (void)configureUI {
  [self configureStackView];
}

- (void)configureStackView {
  _innerStackView = [UIStackView new];
  [self addSubview:self.innerStackView];
  self.innerStackView.axis = UILayoutConstraintAxisVertical;
  self.innerStackView.distribution = UIStackViewDistributionFill;
  self.innerStackView.alignment = UIStackViewAlignmentFill;
  self.innerStackView.spacing = kSWTCharacteristicsStackView_Spacing;
  
  self.innerStackView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.innerStackView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor],
    [self.innerStackView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor],
    [self.innerStackView.topAnchor constraintEqualToAnchor:self.topAnchor],
    [self.innerStackView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor],
  ]];
}

- (void)adjustStackSizeWithSize:(NSUInteger)size {
  NSInteger diff = self.innerStackView.arrangedSubviews.count - size;
  if (diff < 0) {
    [self stackView:self.innerStackView pushViews:labs(diff) ofType:[SWTCharacteristicsView class]];
  } else if (diff > 0) {
    [self stackView:self.innerStackView popItems:diff];
  }
}

- (void)stackView:(UIStackView *)stackView popItems:(NSUInteger)numberOfItems {
  NSUInteger location = (stackView.arrangedSubviews.count - 1) - numberOfItems;
  NSArray<UIView *> *viewsToPop = [stackView.arrangedSubviews
                                   subarrayWithRange:NSMakeRange(location, numberOfItems)];
  for (UIView *viewToPop in viewsToPop) {
    [stackView removeArrangedSubview:viewToPop];
  }
}

- (void)stackView:(UIStackView *)stackView pushViews:(NSUInteger)numberOfItems ofType:(Class)type {
  for (int i = 0; i < numberOfItems; i++) {
    UIView *arrangedSubview = [type new];
    [stackView addArrangedSubview:arrangedSubview];
  }
}

@end

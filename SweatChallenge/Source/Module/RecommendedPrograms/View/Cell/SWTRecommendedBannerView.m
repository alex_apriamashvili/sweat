//
//  SWTRecommendedBannerView.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 14/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Style/Style.h>
#import <SDWebImage/SDWebImage.h>
#import "RecommendedProgramsModule_Internal.h"

#import "SWTRecommendedBannerView.h"
#import "SWTDropIndicator.h"
#import "SWTCharacteristicsStackView.h"
#import "TTGTextTagCollectionView.h"


@interface SWTRecommendedBannerView()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) SWTDropIndicator *difficultyIndicatorView;
@property (nonatomic, strong) SWTCharacteristicsStackView *characteristicsStackView;
@property (nonatomic, strong) TTGTextTagCollectionView *tagView;
@property (nonatomic, strong) UIImageView *trainerImageView;

@end

const CGFloat _kSWTRecommendedBannerView_EdgeOffset = 16.0f;
const CGFloat _kSWTRecommendedBannerView_BottomOffset = 23.0f;
const CGFloat _kSWTRecommendedBannerView_TopOffset = 8.0f;
const CGFloat _kSWTRecommendedBannerView_TagsTopOffset = 16.0f;
const CGFloat _kSWTRecommendedBannerView_Element2ElementOffset = 4.0f;
const CGFloat _kSWTRecommendedBannerView_TitleFontSize = 18.0f;
const CGFloat _kSWTRecommendedBannerView_SubtitleFontSize = 12.0f;
const CGFloat _kSWTRecommendedBannerView_TagFontSize = 8.0f;
const CGFloat _kSWTRecommendedBannerView_TagExtraSpace = 8.0f;
const CGFloat _kSWTRecommendedBannerView_TagCornerRadii = 4.0f;
const CGFloat _kSWTRecommendedBannerView_ImageMaxWidth = 110.0f;

@implementation SWTRecommendedBannerView

#pragma mark - Initialization

+ (instancetype)new {
  SWTRecommendedBannerView *banner = [[SWTRecommendedBannerView alloc] initWithFrame:CGRectZero];
  [banner createContents];
  [banner constrainContents];
  return banner;
}

#pragma mark - Public

- (void)applyViewModel:(id<RecommendedBannerViewModel>)viewModel
                 style:(id<SWTStyle>)style {
  [self applyStyle:style];
  self.titleLabel.text = viewModel.title;
  self.subtitleLabel.text = viewModel.subtitle;
  [self.difficultyIndicatorView fillDropsInAmountOf:viewModel.difficulty];
  [self.characteristicsStackView updateWithCharacteristics:viewModel.characteristics style:style];
  [self.tagView removeAllTags];
  [self.tagView addTags:viewModel.tags];
  [self.trainerImageView sd_setImageWithURL:[NSURL URLWithString:viewModel.imageURLString]];
}

#pragma mark - Content Creation

- (void)createContents {
  [self createTitleLabel];
  [self createSubtitleLabel];
  [self createDifficultyIndicator];
  [self createCharacteristicsStackView];
  [self createTagCloud];
  [self createTrainerImageView];
}

- (void)createTitleLabel {
  _titleLabel = [UILabel new];
  [self addSubview:self.titleLabel];
  self.titleLabel.numberOfLines = 0;
  self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
}

- (void)createSubtitleLabel {
  _subtitleLabel = [UILabel new];
  [self addSubview:self.subtitleLabel];
  self.subtitleLabel.numberOfLines = 0;
  self.subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
}

- (void)createTrainerImageView {
  _trainerImageView = [UIImageView new];
  [self addSubview:self.trainerImageView];
  self.trainerImageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)createDifficultyIndicator {
  _difficultyIndicatorView = [SWTDropIndicator new];
  [self addSubview:self.difficultyIndicatorView];
}

- (void)createCharacteristicsStackView {
  _characteristicsStackView = [SWTCharacteristicsStackView new];
  [self addSubview:self.characteristicsStackView];
}

- (void)createTagCloud {
  _tagView = [TTGTextTagCollectionView new];
  self.tagView.enableTagSelection = NO;
  [self addSubview:self.tagView];
}

#pragma mark - Styling

- (void)applyStyle:(id<SWTStyle>)style {
  [self configureTitleLabelWithStyle:style];
  [self configureSubtitleLabelWithStyle:style];
  [self configureTagCloudWithStyle:style];
}

- (void)configureTitleLabelWithStyle:(id<SWTStyle>)style {
  self.titleLabel.font = [style.font fontOfSize:_kSWTRecommendedBannerView_TitleFontSize weight:kSWTFontWeightMedium];
  self.titleLabel.textColor = style.color.pink;
}

- (void)configureSubtitleLabelWithStyle:(id<SWTStyle>)style {
  self.subtitleLabel.font = [style.font fontOfSize:_kSWTRecommendedBannerView_SubtitleFontSize weight:kSWTFontWeightMedium];
  self.subtitleLabel.textColor = style.color.gray;
}

- (void)configureTagCloudWithStyle:(id<SWTStyle>)style {
  TTGTextTagConfig *config = [TTGTextTagConfig new];
  config.textFont = [style.font fontOfSize:_kSWTRecommendedBannerView_TagFontSize weight:kSWTFontWeightMedium];
  config.textColor = style.color.lightGray;
  config.borderColor = style.color.lightGray;
  config.borderWidth = 1 / [UIScreen mainScreen].scale;
  config.cornerRadius = _kSWTRecommendedBannerView_TagCornerRadii;
  config.shadowColor = UIColor.clearColor;
  config.shadowOffset = CGSizeZero;
  config.shadowRadius = 0;
  config.shadowOpacity = 0;
  config.extraSpace = CGSizeMake(_kSWTRecommendedBannerView_TagExtraSpace, _kSWTRecommendedBannerView_TagExtraSpace);
  config.backgroundColor = UIColor.whiteColor;
  [self.tagView setDefaultConfig:config];
}

#pragma mark - Layout

- (void)constrainContents {
  self.translatesAutoresizingMaskIntoConstraints = NO;
  [self constrainTitleLabel];
  [self constrainSubtitleLabel];
  [self constrainDifficultyIndicator];
  [self constrainCharacteristicsStackView];
  [self constrainTagView];
  [self constrainTrainerImageView];
}

- (void)constrainTitleLabel {
  self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:_kSWTRecommendedBannerView_EdgeOffset],
    [self.titleLabel.topAnchor constraintEqualToAnchor:self.layoutMarginsGuide.topAnchor constant:_kSWTRecommendedBannerView_TopOffset],
  ]];
}

- (void)constrainSubtitleLabel {
  self.subtitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.subtitleLabel.leadingAnchor constraintEqualToAnchor:self.titleLabel.leadingAnchor],
    [self.subtitleLabel.trailingAnchor constraintEqualToAnchor:self.titleLabel.trailingAnchor],
    [self.subtitleLabel.topAnchor constraintEqualToAnchor:self.titleLabel.bottomAnchor],
  ]];
}

- (void)constrainDifficultyIndicator {
  self.difficultyIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.difficultyIndicatorView.leadingAnchor constraintEqualToAnchor:self.subtitleLabel.leadingAnchor],
    [self.difficultyIndicatorView.topAnchor constraintEqualToAnchor:self.subtitleLabel.bottomAnchor constant:_kSWTRecommendedBannerView_Element2ElementOffset],
  ]];
}

- (void)constrainCharacteristicsStackView {
  self.characteristicsStackView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.characteristicsStackView.leadingAnchor constraintEqualToAnchor:self.difficultyIndicatorView.leadingAnchor],
    [self.characteristicsStackView.topAnchor constraintEqualToAnchor:self.difficultyIndicatorView.bottomAnchor constant:_kSWTRecommendedBannerView_TopOffset],
    [self.characteristicsStackView.trailingAnchor constraintEqualToAnchor:self.titleLabel.trailingAnchor]
  ]];
}

- (void)constrainTagView {
  self.tagView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.tagView.leadingAnchor constraintEqualToAnchor:self.characteristicsStackView.leadingAnchor],
    [self.tagView.trailingAnchor constraintEqualToAnchor:self.characteristicsStackView.trailingAnchor],
    [self.tagView.topAnchor constraintEqualToAnchor:self.characteristicsStackView.bottomAnchor constant:_kSWTRecommendedBannerView_TagsTopOffset],
    [self.tagView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-_kSWTRecommendedBannerView_BottomOffset],
  ]];
}

- (void)constrainTrainerImageView {
  self.trainerImageView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.trainerImageView.topAnchor constraintEqualToAnchor:self.titleLabel.topAnchor],
    [self.trainerImageView.bottomAnchor constraintLessThanOrEqualToAnchor:self.tagView.bottomAnchor constant:-_kSWTRecommendedBannerView_EdgeOffset],
    [self.trainerImageView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-_kSWTRecommendedBannerView_EdgeOffset],
    [self.trainerImageView.leadingAnchor constraintEqualToAnchor:self.titleLabel.trailingAnchor],
    [self.trainerImageView.widthAnchor constraintLessThanOrEqualToConstant:_kSWTRecommendedBannerView_ImageMaxWidth],
  ]];
}

@end

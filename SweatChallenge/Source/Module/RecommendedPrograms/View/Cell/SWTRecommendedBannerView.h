//
//  SWTRecommendedBannerView.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 14/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWTStyle, RecommendedBannerViewModel;

NS_ASSUME_NONNULL_BEGIN

@interface SWTRecommendedBannerView : UIView

- (void)applyViewModel:(id<RecommendedBannerViewModel>)viewModel
                 style:(id<SWTStyle>)style;

@end

NS_ASSUME_NONNULL_END

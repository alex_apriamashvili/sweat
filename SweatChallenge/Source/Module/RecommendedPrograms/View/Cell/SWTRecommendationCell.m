//
//  SWTRecommendationCell.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Style/Style.h>

#import "SWTRecommendationCell.h"
#import "SWTRecommendedBannerView.h"

@interface SWTRecommendationCell()

@property (nonatomic, strong) SWTRecommendedBannerView *bannerView;

@end

const CGFloat _kBannerHorizontalEdgeOffset = 20.0f;
const CGFloat _kBannerVerticalEdgeOffset = 9.0f;
const CGFloat _kBannerMaxHeight = 512.0f;
const CGFloat _kBannerCornerRadii = 6.0f;
const CGFloat _kBannerShadowRadii = 1.0f;

@implementation SWTRecommendationCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self setUp];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super initWithCoder:coder];
  if (self) {
    [self setUp];
  }
  return self;
}

#pragma mark - Public

- (void)applyViewModel:(id<RecommendedBannerViewModel>)viewModel
                 style:(id<SWTStyle>)style {
  self.bannerView.layer.borderColor = style.color.veryLightGray.CGColor;
  [self.bannerView applyViewModel:viewModel style:style];
}

#pragma mark - Private

- (void)setUp {
  [self configureUI];
  [self constrainContents];
}

- (void)configureUI {
  self.backgroundColor = UIColor.clearColor;
  [self dropShadow];
  [self configureBannerView];
}

- (void)dropShadow {
  self.layer.shadowRadius = _kBannerShadowRadii;
  self.layer.shadowOffset = CGSizeMake(0, 1);
  self.layer.shadowOpacity = 0.3;
  self.layer.shouldRasterize = YES;
  self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (void)configureBannerView {
  _bannerView = [SWTRecommendedBannerView new];
  [self.contentView addSubview:self.bannerView];
  self.bannerView.backgroundColor = UIColor.whiteColor;
  self.bannerView.layer.cornerRadius = _kBannerCornerRadii;
  self.bannerView.layer.borderWidth = 1 / [UIScreen mainScreen].scale;
  self.bannerView.clipsToBounds = YES;
}

#pragma mark - Private (Layout)

- (void)constrainContents {
  self.translatesAutoresizingMaskIntoConstraints = NO;
  [self constrainBannerView];
}

- (void)constrainBannerView {
  self.bannerView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.bannerView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:_kBannerHorizontalEdgeOffset],
    [self.bannerView.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-_kBannerHorizontalEdgeOffset],
    [self.bannerView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:_kBannerVerticalEdgeOffset],
    [self.bannerView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-_kBannerVerticalEdgeOffset]
  ]];
}

@end

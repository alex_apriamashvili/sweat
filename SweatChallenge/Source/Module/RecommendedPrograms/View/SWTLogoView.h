//
//  LogoView.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 29/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWTLogoView : UIView

+ (instancetype)fullsizedLogo;

@end

NS_ASSUME_NONNULL_END

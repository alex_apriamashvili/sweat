//
//  LogoView.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 29/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTLogoView.h"

@interface SWTLogoView()

@property (nonatomic, strong) UIImageView *logoImageView;

@end

NSString * const _kSWTLogoView_ImageName = @"sweat-logo";

@implementation SWTLogoView

+ (instancetype)fullsizedLogo {
  CGSize imageSize = [UIImage imageNamed:_kSWTLogoView_ImageName].size;
  return [[SWTLogoView alloc] initWithFrame:CGRectMake(0, 0, imageSize.width, imageSize.height)];
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self configureUI];
  }
  return self;
}

- (void)configureUI {
  UIImage *logo = [UIImage imageNamed:_kSWTLogoView_ImageName];
  _logoImageView = [[UIImageView alloc] initWithImage:logo];
  [self addSubview:self.logoImageView];
}

- (void)constrainContents {
  self.logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.logoImageView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor],
    [self.logoImageView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor],
    [self.logoImageView.topAnchor constraintEqualToAnchor:self.topAnchor],
    [self.logoImageView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor],
  ]];
}

@end

//
//  SWTRecommendedProgramsViewController.m
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTRecommendedProgramsViewController.h"
#import "RecommendedProgramsModule_Constants.h"
#import "SWTRecommendedProgramsDelegate.h"
#import "SWTRecommendedProgramsHeader.h"
#import "SWTRecommendationCell.h"
#import "SWTLogoView.h"

@interface SWTRecommendedProgramsViewController ()

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) SWTRecommendedProgramsDelegate *collectionDelegate;
@property (nonatomic, strong) id<SWTStyle> style;

@end

@implementation SWTRecommendedProgramsViewController

#pragma mark - Initialization

+ (instancetype)viewWithStyle:(id<SWTStyle>)style {
  SWTRecommendedProgramsViewController *vc = [SWTRecommendedProgramsViewController new];
  vc->_style = style;
  return vc;
}

#pragma mark - LifeCycle

- (void)viewDidLoad {
  [super viewDidLoad];
  [self configureUI];
  [self constrainContents];
  [self.output viewIsReady];
  [self.output loadData];
}

#pragma mark - -getters / -setters

- (SWTRecommendedProgramsDelegate *)collectionDelegate {
  if (_collectionDelegate != nil) { return _collectionDelegate; }
  _collectionDelegate = [SWTRecommendedProgramsDelegate delegateWithActionHandler:self style:self.style];
  return _collectionDelegate;
}

#pragma mark - RecommendedProgramsModule Implementation

- (UIViewController *)viewController {
  return self;
}

- (id<RecommendedProgramsModuleInput>)input {
  if ([self.output conformsToProtocol:@protocol(RecommendedProgramsModuleInput)]) {
    return (id<RecommendedProgramsModuleInput>)self.output;
  }
  return nil;
}

#pragma mark - RecommendedProgramsViewInput Implementation

- (void)updateGroupTitle:(NSString *)string {
  [self.collectionDelegate updateGroupTitle:string];
  [self.collectionView reloadData];
}

- (void)updateContents:(NSArray<RecommendedBannerViewModel> *)contents {
  [self.collectionDelegate updateContents:contents];
  [self.collectionView reloadData];
}

#pragma mark - Private

- (void)configureUI {
  self.navigationController.navigationBar.translucent = NO;
  self.navigationItem.titleView = [SWTLogoView fullsizedLogo];
  [self configureCollectionView];
}

- (void)configureCollectionView {
  UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
  layout.minimumInteritemSpacing = 0;
  layout.minimumLineSpacing = 0;
  _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                       collectionViewLayout:layout];
  [self.view addSubview:self.collectionView];
  [self registerCollectionItems];
  self.collectionView.backgroundColor = UIColor.whiteColor;
  self.collectionView.delegate = self.collectionDelegate;
  self.collectionView.dataSource = self.collectionDelegate;
}

- (void)registerCollectionItems {
  [self.collectionView registerClass:[SWTRecommendationCell class]
          forCellWithReuseIdentifier:RECOMMENDED_PROGRAMS_CELL_IDENTIFIER];
  [self.collectionView registerClass:[SWTRecommendedProgramsHeader class]
          forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                 withReuseIdentifier:RECOMMENDED_PROGRAMS_HEADER_IDENTIFIER];
}

#pragma mark - Private (Layout)

- (void)constrainContents {
  [self constrainCollectionView];
}

- (void)constrainCollectionView {
  self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.collectionView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
    [self.collectionView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
    [self.collectionView.topAnchor constraintEqualToAnchor:self.view.topAnchor],
    [self.collectionView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
  ]];
}

@end

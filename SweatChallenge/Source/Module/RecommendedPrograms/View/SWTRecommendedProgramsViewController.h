//
//  SWTRecommendedProgramsViewController.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecommendedProgramsModule.h"
#import "RecommendedProgramsModule_Internal.h"

typedef UIViewController<RecommendedProgramsModule, RecommendedProgramsViewInput, CollectionActionHandler> RecommendedProgramsView;

NS_ASSUME_NONNULL_BEGIN

@interface SWTRecommendedProgramsViewController : RecommendedProgramsView

/// a reference to the presentation layer
@property (nonatomic, strong, nonnull) id<RecommendedProgramsViewOutput> output;

/// create view layer with predefined style
/// @param style a style-sheet that contains fonts and colors for the app
+ (instancetype)viewWithStyle:(id<SWTStyle>)style;

@end

NS_ASSUME_NONNULL_END

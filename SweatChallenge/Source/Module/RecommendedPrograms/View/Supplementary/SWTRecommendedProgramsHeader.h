//
//  SWTRecommendedProgramsHeader.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWTStyle;

NS_ASSUME_NONNULL_BEGIN

@interface SWTRecommendedProgramsHeader : UICollectionReusableView

- (void)updateTitle:(NSString *)title style:(id<SWTStyle>)style;

@end

NS_ASSUME_NONNULL_END

//
//  SWTRecommendedProgramsHeader.m
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Style/Style.h>
#import "SWTRecommendedProgramsHeader.h"

@interface SWTRecommendedProgramsHeader()

@property (nonatomic, strong) UILabel *title;

@end

const CGFloat _kSWTRecommendedProgramsHeader_TitleEdgeOffset = 21.0f;
const CGFloat _kSWTRecommendedProgramsHeader_FontSize = 18.0f;
const CGFloat _kSWTRecommendedProgramsHeader_FontSpacing = 1.5;

@implementation SWTRecommendedProgramsHeader

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self setUp];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super initWithCoder:coder];
  if (self) {
    [self setUp];
  }
  return self;
}

#pragma mark - Public

- (void)updateTitle:(NSString *)title style:(id<SWTStyle>)style {
  if ([self.title.text isEqualToString:title]) { return; }
  self.title.attributedText = [self attributedTitleWithString:title style:style];
  [self setNeedsLayout];
}

#pragma mark - Private

- (void)setUp {
  [self configureUI];
  [self constrainContents];
}

- (void)configureUI {
  [self configureTitleLabel];
}

- (void)configureTitleLabel {
  _title = [UILabel new];
  UILabel *label = [UILabel new];

  NSMutableAttributedString *text = [[NSMutableAttributedString alloc]
                                     initWithString:@"127"];

  [text addAttribute:NSKernAttributeName
               value:@-0.5
               range:NSMakeRange(0, text.length)];

  [label setAttributedText:text];
  [self addSubview:self.title];
}

- (NSAttributedString *)attributedTitleWithString:(NSString *)string style:(id<SWTStyle>)style {
  NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:string];
  NSDictionary<NSAttributedStringKey, id> *attributes = @{
               NSKernAttributeName : @(_kSWTRecommendedProgramsHeader_FontSpacing),
               NSFontAttributeName : [style.font fontOfSize:_kSWTRecommendedProgramsHeader_FontSize weight:kSWTFontWeightBold],
    NSForegroundColorAttributeName : style.color.black,
  };
  [text addAttributes:attributes range:NSMakeRange(0, text.length)];
  return [text copy];
}

#pragma mark - Private (Layout)

- (void)constrainContents {
  [self constrainTitleLabel];
}

- (void)constrainTitleLabel {
  self.title.translatesAutoresizingMaskIntoConstraints = NO;
  [NSLayoutConstraint activateConstraints:@[
    [self.title.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:_kSWTRecommendedProgramsHeader_TitleEdgeOffset],
    [self.title.topAnchor constraintEqualToAnchor:self.topAnchor constant:_kSWTRecommendedProgramsHeader_TitleEdgeOffset],
    [self.title.trailingAnchor constraintLessThanOrEqualToAnchor:self.trailingAnchor constant:-_kSWTRecommendedProgramsHeader_TitleEdgeOffset]
  ]];
  
}

@end

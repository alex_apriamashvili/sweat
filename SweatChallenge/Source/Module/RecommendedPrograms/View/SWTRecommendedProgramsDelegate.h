//
//  SWTRecommendedProgramsDelegate.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "RecommendedProgramsModule_Internal.h"

@import UIKit;

@protocol SWTStyle;

NS_ASSUME_NONNULL_BEGIN

@interface SWTRecommendedProgramsDelegate : NSObject<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

/// create an instance of collection-view delegate/datasource with predefined action-handler and style
/// @param actionHandler an entity that is responsible for handling the actions hapenning on a collection view
/// @param style a style-sheet that contains fonts and colors for the app
+ (instancetype)delegateWithActionHandler:(nullable id<CollectionActionHandler>)actionHandler
                                    style:(id<SWTStyle>)style;

/// apply new set of view-models
/// @param viewModels a set of view-models to be presented in collection view
- (void)updateContents:(NSArray<RecommendedBannerViewModel> *)viewModels;

/// apply new group title value
/// @param groupTitle a value of the group-title that needs to be presented in collection view
- (void)updateGroupTitle:(NSString *)groupTitle;

@end

NS_ASSUME_NONNULL_END

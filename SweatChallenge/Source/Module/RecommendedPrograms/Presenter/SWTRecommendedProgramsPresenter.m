//
//  SWTRecommendedProgramsPresenter.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTRecommendedProgramsPresenter.h"
#import <ProgramService/ProgramService.h>
#import "SWTRecommendedItemViewModel.h"
#import "SWTCharacteristicsViewModel.h"

@interface SWTRecommendedProgramsPresenter()

@property (nonatomic, strong, readonly) id<ProgramService> service;

@end

NSString * const _kSWTRecommendedProgramsPresenter_GroupTitle = @"Recommended for you";
NSString * const _kSWTRecommendedProgramsPresenter_IntensityAttr = @"intensity";

@implementation SWTRecommendedProgramsPresenter

+ (instancetype)presenterWithService:(id<ProgramService>)service {
  SWTRecommendedProgramsPresenter *presenter = [SWTRecommendedProgramsPresenter new];
  presenter->_service = service;
  return presenter;
}

#pragma mark - RecommendedProgramsViewOutput Implementation

- (void)viewIsReady {
  [self.view updateGroupTitle:_kSWTRecommendedProgramsPresenter_GroupTitle];
}

- (void)loadData {
  __weak typeof(self) weakSelf = self;
  [self.service requestRecommendedProgramsWithCompletionBlock:^(NSArray<ProgramPreview> *modelList) {
    __strong typeof(weakSelf) strongSelf = weakSelf;
    NSArray<RecommendedBannerViewModel> *viewModels = [strongSelf mapDataToViewModelList:modelList];
    [strongSelf.view updateContents:viewModels];
  }];
}

#pragma mark - Private

- (NSArray<RecommendedBannerViewModel> *)mapDataToViewModelList:(NSArray<ProgramPreview> *)modelList {
  NSMutableArray *viewModels = [NSMutableArray array];
  for (id<ProgramPreview> model in modelList) {
    [viewModels addObject:
     [SWTRecommendedItemViewModel viewModelWithTitle:model.name
                                            subtitle:[NSString stringWithFormat:@"with %@", model.trainer.name]
                                          difficulty:[self intensityFromAttributes:model.attributes]
                                     characteristics:[self characteristicsFromAttributes:model.attributes]
                                                tags:[self tagsFromModels:model.tags]
                                           urlString:model.trainer.imageUrlString]
     ];
  }
  return [viewModels copy];
}

- (NSUInteger)intensityFromAttributes:(NSArray<ProgramAttribute> *)attributes {
  for (id<ProgramAttribute> attr in attributes) {
    if ([attr.codeName isEqualToString:_kSWTRecommendedProgramsPresenter_IntensityAttr]) {
      return (NSUInteger)attr.value;
    }
  }
  return 0;
}

- (NSArray<CharacteristicsViewModel> *)characteristicsFromAttributes:(NSArray<ProgramAttribute> *)attributes {
  NSMutableArray *attributeList = [NSMutableArray array];
  for (id<ProgramAttribute> attr in attributes) {
    if ([attr.codeName isEqualToString:_kSWTRecommendedProgramsPresenter_IntensityAttr]) { continue; }
    double percentage = attr.total > 0 ? attr.value / attr.total : 0;
    [attributeList addObject:
     [SWTCharacteristicsViewModel viewModelWithName:attr.name
                                         percentage:percentage]
     ];
  }
  return [attributeList copy];
}

- (NSArray<NSString *> *)tagsFromModels:(NSArray<Tag> *)tags {
  NSMutableArray *tagList = [NSMutableArray array];
  for (id<Tag> tag in tags) {
    [tagList addObject:tag.name];
  }
  return [tagList copy];
}

@end

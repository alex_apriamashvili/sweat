//
//  SWTCharacteristicsViewModel.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 15/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTCharacteristicsViewModel.h"

@implementation SWTCharacteristicsViewModel

@synthesize name, percentage;

+ (instancetype)viewModelWithName:(NSString *)name
                       percentage:(CGFloat)percentage {
  SWTCharacteristicsViewModel *vm = [SWTCharacteristicsViewModel new];
  
  vm->name = [name copy];
  vm->percentage = percentage;
  
  return vm;
}

@end

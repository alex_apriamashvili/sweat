//
//  SWTRecommendedItemViewModel.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 15/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import Foundation;
#import "RecommendedProgramsModule_Internal.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWTRecommendedItemViewModel : NSObject<RecommendedBannerViewModel>

+ (instancetype)viewModelWithTitle:(NSString *)title
                          subtitle:(NSString *)subtitle
                        difficulty:(NSUInteger)difficulty
                   characteristics:(NSArray<id<CharacteristicsViewModel>> *)characteristics
                              tags:(NSArray<NSString *> *)tags
                         urlString:(NSString *)urlString;

@end

NS_ASSUME_NONNULL_END

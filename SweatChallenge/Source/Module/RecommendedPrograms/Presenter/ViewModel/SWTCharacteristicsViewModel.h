//
//  SWTCharacteristicsViewModel.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 15/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import UIKit;
#import "RecommendedProgramsModule_Internal.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWTCharacteristicsViewModel : NSObject <CharacteristicsViewModel>

+ (instancetype)viewModelWithName:(NSString *)name
                       percentage:(CGFloat)percentage;

@end

NS_ASSUME_NONNULL_END

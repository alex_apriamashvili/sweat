//
//  SWTRecommendedItemViewModel.m
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 15/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTRecommendedItemViewModel.h"

@implementation SWTRecommendedItemViewModel

@synthesize title, subtitle, difficulty, characteristics, tags, imageURLString;

+ (instancetype)viewModelWithTitle:(NSString *)title
                         subtitle:(NSString *)subtitle
                       difficulty:(NSUInteger)difficulty
                  characteristics:(NSArray<id<CharacteristicsViewModel>> *)characteristics
                              tags:(NSArray<NSString *> *)tags
                         urlString:(NSString *)urlString {
  SWTRecommendedItemViewModel *vm = [SWTRecommendedItemViewModel new];
  
  vm->title = [title copy];
  vm->subtitle = [subtitle copy];
  vm->difficulty = difficulty;
  vm->characteristics = [characteristics copy];
  vm->tags = [tags copy];
  vm->imageURLString = [urlString copy];
  
  return vm;
}

@end

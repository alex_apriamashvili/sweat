//
//  SWTRecommendedProgramsPresenter.h
//  RecommendedPrograms
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecommendedProgramsModule.h"
#import "RecommendedProgramsModule_Internal.h"

@protocol ProgramService;

NS_ASSUME_NONNULL_BEGIN

typedef NSObject<RecommendedProgramsModuleInput, RecommendedProgramsViewOutput> RecommendedProgramsPresenter;

@interface SWTRecommendedProgramsPresenter : RecommendedProgramsPresenter

/// a weak reference to a view layer
@property (nonatomic, nullable, weak) id<RecommendedProgramsViewInput> view;

/// create presenter with a predefined service
/// @param service  data-source for this module
+ (instancetype)presenterWithService:(id<ProgramService>)service;

@end

NS_ASSUME_NONNULL_END

//
//  SWTRecommendedProgramsAssembly.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import Foundation;
@import UIKit;

@protocol RecommendedProgramsModule, RecommendedProgramsDependency;

NS_ASSUME_NONNULL_BEGIN

@interface SWTRecommendedProgramsAssembly : NSObject

/// assemble `RecommendedPrograms` module with the given set of dependencies
/// @param dependency data that needs to be injected into the module upon creation
- (NSObject<RecommendedProgramsModule> *)assembleWithDependency:(nullable NSObject<RecommendedProgramsDependency> *)dependency;

@end

NS_ASSUME_NONNULL_END

//
//  SWTRecommendedProgramsAssembly.m
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "SWTRecommendedProgramsAssembly.h"
#import "RecommendedProgramsModule_Internal.h"
#import "SWTRecommendedProgramsViewController.h"
#import "SWTRecommendedProgramsPresenter.h"

@import UIKit;

@implementation SWTRecommendedProgramsAssembly

- (NSObject<RecommendedProgramsModule> *)assembleWithDependency:(nullable NSObject<RecommendedProgramsDependency> *)dependency {
  SWTRecommendedProgramsViewController *view = [SWTRecommendedProgramsViewController viewWithStyle:dependency.style];
  SWTRecommendedProgramsPresenter *presenter = [SWTRecommendedProgramsPresenter presenterWithService:dependency.service];
  view.output = presenter;
  presenter.view = view;
  return view;
}

@end

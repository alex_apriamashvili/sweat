//
//  RecommendedProgramsModule_Constants.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

/// programs cell identifier
#define RECOMMENDED_PROGRAMS_CELL_IDENTIFIER @"com.sweat.challange.recommended.programs.cell"

/// programs group header identifier
#define RECOMMENDED_PROGRAMS_HEADER_IDENTIFIER @"com.sweat.challange.recommended.programs.header"

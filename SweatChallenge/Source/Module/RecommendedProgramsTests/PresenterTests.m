//
//  PresenterTests.m
//  SweatChallengeTests
//
//  Created by Alex Apriamashvili on 04/01/2020.
//  Copyright © 2020 Alex Apriamashvili. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SWTRecommendedProgramsPresenter.h"
#import "ProgramServiceMock.h"
#import "ViewInputMock.h"

@interface PresenterTests : XCTestCase

@property (nonatomic, strong) SWTRecommendedProgramsPresenter *sut;
@property (nonatomic, strong) ProgramServiceMock *mockService;
@property (nonatomic, strong) ViewInputMock *mockView;

@end

@implementation PresenterTests

- (NSArray<ProgramPreview> *)stubbedPrograms {
  return (NSArray<ProgramPreview> *)@[[ProgramPreviewStub stub]];
}

- (void)setUp {
  [super setUp];
  _mockService = [ProgramServiceMock new];
  _mockView = [ViewInputMock new];
  _sut = [SWTRecommendedProgramsPresenter presenterWithService:self.mockService];
  self.sut.view = self.mockView;
}

- (void)tearDown {
  _sut = nil;
  _mockView = nil;
  _mockService = nil;
  [super tearDown];
}

- (void)testCallUpdateTitleOnViewLayerOnceViewIsReadySignalIsReceived {
  [self.sut viewIsReady];
  XCTAssert(self.mockView.didCall_updateGroupTitle);
}

- (void)testAskServiceToFetchProgramsOnceLoadDataSignalIsReceived {
  [self.sut loadData];
  XCTAssert(self.mockService.didCall_requestRecommendedProgramsWithCompletionBlock);
}

- (void)testCallUpdateContentsOnViewOnceServiceReturnsResult {
  [self.mockService setStubRecommendedProgramList:self.stubbedPrograms];
  [self.sut loadData];
  XCTAssert(self.mockView.didCall_updateContents);
}

- (void)testContentsNumberEqualsToNumbersOfPrograms {
  [self.mockService setStubRecommendedProgramList:self.stubbedPrograms];
  [self.sut loadData];
  XCTAssertEqual(self.stubbedPrograms.count, self.mockView.receivedContents.count);
}

@end

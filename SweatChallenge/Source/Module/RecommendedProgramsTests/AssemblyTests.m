//
//  AssemblyTests.m
//  SweatChallengeTests
//
//  Created by Alex Apriamashvili on 29/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DependencyMock.h"
#import "SWTRecommendedProgramsViewController.h"
#import "SWTRecommendedProgramsPresenter.h"

@interface AssemblyTests : XCTestCase

@property (nonatomic, strong) SWTRecommendedProgramsAssembly *sut;
@property (nonatomic, strong) DependencyMock *mockDependency;

@end

@implementation AssemblyTests

- (void)setUp {
  [super setUp];
  _sut = [SWTRecommendedProgramsAssembly new];
  _mockDependency = [DependencyMock new];
}

- (void)tearDown {
  _mockDependency = nil;
  _sut = nil;
  [super tearDown];
}

- (void)tesRecommendedProgramstModuleIsAssembled {
  NSObject<RecommendedProgramsModule> *result = [self.sut assembleWithDependency:self.mockDependency];
  XCTAssertNotNil(result);
}

- (void)testViewLayerIsCreated {
  NSObject<RecommendedProgramsModule> *result = [self.sut assembleWithDependency:self.mockDependency];
  XCTAssertNotNil(result.viewController);
}

- (void)testModuleInputIsCreated {
  NSObject<RecommendedProgramsModule> *result = [self.sut assembleWithDependency:self.mockDependency];
  XCTAssertNotNil(result.input);
}

- (void)testModuleIsConformedBySWTRecommendedProgramsViewController {
  NSObject<RecommendedProgramsModule> *result = [self.sut assembleWithDependency:self.mockDependency];
  XCTAssert([result isMemberOfClass:[SWTRecommendedProgramsViewController class]]);
}

- (void)testViewOutputIsAssigned {
  NSObject<RecommendedProgramsModule> *result = [self.sut assembleWithDependency:self.mockDependency];
  SWTRecommendedProgramsViewController *viewLayer = (SWTRecommendedProgramsViewController *)result.viewController;
  XCTAssertNotNil(viewLayer.output);
}

- (void)testViewInputIsAssigned {
  NSObject<RecommendedProgramsModule> *result = [self.sut assembleWithDependency:self.mockDependency];
  SWTRecommendedProgramsPresenter *presenter = (SWTRecommendedProgramsPresenter *)result.input;
  XCTAssertNotNil(presenter.view);
}

@end

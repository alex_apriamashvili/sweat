//
//  ProgramServiceMock.m
//  RecommendedProgramsTests
//
//  Created by Alex Apriamashvili on 04/01/2020.
//  Copyright © 2020 Alex Apriamashvili. All rights reserved.
//

#import "ProgramServiceMock.h"

NSString * const _kProgramServiceStubString = @"com.sweat.recommended.program.tests.stub";

@implementation ProgramServiceMock

- (void)requestRecommendedProgramsWithCompletionBlock:(ProgramPreviewResponseBlock)block {
  _didCall_requestRecommendedProgramsWithCompletionBlock = YES;
  block(self.stubRecommendedProgramList);
}

@end

@implementation ProgramPreviewStub

@synthesize identifier, name, codeName, acronym, imageUrlString, tags, trainer, attributes;

+ (instancetype)stub {
  return [ProgramPreviewStub previewWithIdentifier:1
                                              name:_kProgramServiceStubString
                                          codeName:_kProgramServiceStubString
                                           acronym:_kProgramServiceStubString
                                    imageUrlString:_kProgramServiceStubString
                                              tags:@[[TagStub stub]]
                                           trainer:[TrainerStub stub]
                                        attributes:@[[ProgramAttributeStub stub]]];
}

+ (instancetype)previewWithIdentifier:(NSInteger)identifier
                                 name:(NSString *)name
                             codeName:(NSString *)codeName
                              acronym:(NSString *)acronym
                       imageUrlString:(NSString *)imageUrlString
                                 tags:(NSArray *)tags
                              trainer:(id)trainer
                           attributes:(NSArray *)attributes {
  ProgramPreviewStub *stub = [ProgramPreviewStub new];
  stub->identifier = identifier;
  stub->name = name;
  stub->codeName = codeName;
  stub->acronym = acronym;
  stub->imageUrlString = imageUrlString;
  stub->tags = [tags copy];
  stub->trainer = trainer;
  stub->attributes = [attributes copy];
  return stub;
}

@end

@implementation ProgramAttributeStub

@synthesize identifier, codeName, name, value, total;

+ (instancetype)stub {
  ProgramAttributeStub *stub = [ProgramAttributeStub new];
  stub->identifier = 1;
  stub->codeName = _kProgramServiceStubString;
  stub->name = _kProgramServiceStubString;
  stub->value = 1;
  stub->total = 3;
  return stub;
}

@end

@implementation TagStub

@synthesize identifier, name;

+ (instancetype)stub {
  TagStub *stub = [TagStub new];
  stub->identifier = 1;
  stub->name = _kProgramServiceStubString;
  return stub;
}

@end

@implementation TrainerStub

@synthesize identifier, codeName, name, imageUrlString;

+ (instancetype)stub {
  TrainerStub *stub = [TrainerStub new];
  stub->identifier = 1;
  stub->codeName = _kProgramServiceStubString;
  stub->name = _kProgramServiceStubString;
  stub->imageUrlString = _kProgramServiceStubString;
  return stub;
}

@end

//
//  ProgramServiceMock.h
//  RecommendedProgramsTests
//
//  Created by Alex Apriamashvili on 04/01/2020.
//  Copyright © 2020 Alex Apriamashvili. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ProgramService/ProgramService.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProgramServiceMock : NSObject<ProgramService>

@property (nonatomic) BOOL didCall_requestRecommendedProgramsWithCompletionBlock;
@property (nonatomic) NSArray<ProgramPreview> *stubRecommendedProgramList;

@end

@interface ProgramPreviewStub : NSObject<ProgramPreview>

+ (instancetype)stub;
+ (instancetype)previewWithIdentifier:(NSInteger)identifier
                                 name:(NSString *)name
                             codeName:(NSString *)codeName
                              acronym:(NSString *)acronym
                       imageUrlString:(NSString *)imageUrlString
                                 tags:(NSArray *)tags
                              trainer:(id)trainer
                           attributes:(NSArray *)attributes;

@end

@interface ProgramAttributeStub : NSObject<ProgramAttribute>

+ (instancetype)stub;

@end

@interface TagStub : NSObject<Tag>

+ (instancetype)stub;

@end

@interface TrainerStub : NSObject<Trainer>

+ (instancetype)stub;

@end


NS_ASSUME_NONNULL_END

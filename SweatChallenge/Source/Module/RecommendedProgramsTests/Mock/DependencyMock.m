//
//  DependencyMock.m
//  RecommendedProgramsTests
//
//  Created by Alex Apriamashvili on 29/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "DependencyMock.h"

@implementation DependencyMock

- (id<SWTStyle>)style {
  return nil;
}

- (id<ProgramService>)service {
  return nil;
}

@end

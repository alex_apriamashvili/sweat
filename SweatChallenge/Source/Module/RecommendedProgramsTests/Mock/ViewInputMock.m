//
//  ViewInputMock.m
//  RecommendedProgramsTests
//
//  Created by Alex Apriamashvili on 04/01/2020.
//  Copyright © 2020 Alex Apriamashvili. All rights reserved.
//

#import "ViewInputMock.h"

@implementation ViewInputMock

- (void)updateContents:(NSArray<RecommendedBannerViewModel> *)contents {
  _didCall_updateContents = YES;
  _receivedContents = contents;
}

- (void)updateGroupTitle:(NSString *)string {
  _didCall_updateGroupTitle = YES;
}

@end

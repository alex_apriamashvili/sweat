//
//  DependencyMock.h
//  RecommendedProgramsTests
//
//  Created by Alex Apriamashvili on 29/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RecommendedPrograms/RecommendedPrograms.h>

NS_ASSUME_NONNULL_BEGIN

@interface DependencyMock : NSObject<RecommendedProgramsDependency>

@end

NS_ASSUME_NONNULL_END

//
//  ViewInputMock.h
//  RecommendedProgramsTests
//
//  Created by Alex Apriamashvili on 04/01/2020.
//  Copyright © 2020 Alex Apriamashvili. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecommendedProgramsModule_Internal.h"

NS_ASSUME_NONNULL_BEGIN

@interface ViewInputMock : NSObject<RecommendedProgramsViewInput>

@property (nonatomic) BOOL didCall_updateContents;
@property (nonatomic, copy, readonly) NSArray<RecommendedBannerViewModel> *receivedContents;
@property (nonatomic) BOOL didCall_updateGroupTitle;

@end

NS_ASSUME_NONNULL_END

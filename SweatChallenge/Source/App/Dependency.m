//
//  Dependency.m
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 09/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "Dependency.h"
#import <Style/Style.h>
#import <ProgramService/ProgramService.h>

@implementation Dependency

@synthesize style, service;

- (id<SWTStyle>)style {
  if (!style) {
    style = [SWTStyleImp new];
  }
  return style;
}

- (id<ProgramService>)service {
  if (!service) {
    service = [StubProgramService new];
  }
  return service;
}

@end

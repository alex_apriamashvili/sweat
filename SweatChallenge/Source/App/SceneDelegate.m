#import "SceneDelegate.h"
#import "AppConfig.h"

@interface SceneDelegate ()

@end

@implementation SceneDelegate

- (void)scene:(UIScene *)scene
willConnectToSession:(UISceneSession *)session
      options:(UISceneConnectionOptions *)connectionOptions  API_AVAILABLE(ios(13.0)){
  if (@available(iOS 13.0, *)) {
    _window = [[AppConfig new] setupApplicationWindowWithScene:scene];
  }
}

@end

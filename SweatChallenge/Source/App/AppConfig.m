//
//  AppConfig.m
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 29/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import "AppConfig.h"
#import <RecommendedPrograms/RecommendedPrograms.h>
#import "Dependency.h"

@implementation AppConfig

- (UIWindow *)setupApplicationWindowWithScene:(UIScene *)scene {
  UIWindow *window = [[UIWindow alloc] initWithWindowScene:(UIWindowScene *)scene];
  window.rootViewController = [self _rootViewController];
  window.overrideUserInterfaceStyle = UIUserInterfaceStyleLight; /* Dark Theme is disabled */
  return window;
}

- (UIWindow *)setupApplicationWindow {
  UIWindow *window = [UIWindow new];
  window.rootViewController = [self _rootViewController];
  return window;
}

- (UIViewController *)_rootViewController {
  id<RecommendedProgramsDependency> dependency = [Dependency new];
  id<RecommendedProgramsModule> recommendedModule = [[SWTRecommendedProgramsAssembly new] assembleWithDependency:dependency];
  return [[UINavigationController alloc] initWithRootViewController:recommendedModule.viewController];
}

@end

//
//  AppDelegate.m
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 08/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//
#import "AppDelegate.h"
#import "AppConfig.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  if (@available(iOS 13.0, *)) {
    /* the initial setup will be handled in SceneDelegate */
  } else {
    window = [[AppConfig new] setupApplicationWindow];
    [window makeKeyAndVisible];
  }
  return YES;
}

@end

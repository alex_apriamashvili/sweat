//
//  AppConfig.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 29/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

@import Foundation;
@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface AppConfig : NSObject

/// create a new application window with predefined root view-controller for a given scene
/// available since *iOS 13* onwards
/// @param scene application scene
- (UIWindow *)setupApplicationWindowWithScene:(UIScene *)scene API_AVAILABLE(ios(13.0));

/// create a new application window with predefined root view-controller
/// available since *iOS 10* onwards
- (UIWindow *)setupApplicationWindow API_AVAILABLE(ios(10.0));

@end

NS_ASSUME_NONNULL_END

//
//  Dependency.h
//  SweatChallenge
//
//  Created by Alex Apriamashvili on 09/12/2019.
//  Copyright © 2019 Alex Apriamashvili. All rights reserved.
//

#import <RecommendedPrograms/RecommendedPrograms.h>

@import Foundation;

@protocol SWTStyle;

NS_ASSUME_NONNULL_BEGIN

@interface Dependency : NSObject<RecommendedProgramsDependency>

@end

NS_ASSUME_NONNULL_END

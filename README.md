### SWEAT CODE CHALLANGE (iOS)

## Installation
To make sure that the environment stays the same across different machines, I have introduced `Gemfile` and `Gemfile.lock` files, where I have fixed the version of `Cocoapods` to `1.8.4` as it is the latest available version for the moment I'm implementing the task.  
Upon the installation of the fore-mentioned version of `Cocoapods` on your machine you might face a problem if you are using **Xcode 11** and **macOS 10.14**. The problem is in the Ruby version that is assumed by **Xcode 11** to be `2.6`, but in fact, being `2.3` as `2.6` is available with **macOS 11.15 (Catalina)** only. You might not have this problem if you're using any Ruby Version Manager available as RVM itself or RBENV. However, if you do use *System Ruby*, please make the following operations to install cocoapods on your machine:
```bash
$ export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/"
$ ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubyhdrdir"]'
```
More information could be found in the following [issue](https://github.com/castwide/vscode-solargraph/issues/78)  
Once you have set your headers dir to a correct value, you can install `Cocoapods`. To do that, please, execute the following command in local your working copy root directory:
```bash
$ bundle install
```
If you don't have `bundler` installed on your machine, please, execute the following:
```bash
$ gem install bundler -v '2.0.1'
```
Once you've installed `Cocoapods`, please, install iOS project dependencies with with the following command:
```bash
 bundle exec pod install
```